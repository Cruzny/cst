Final Project
---
 
Create a web app that allows the user to see historical information on NBA teams

 


1. Display all the information on all the teams 
2. Display the top five oldest teams 
3. Display the top five most recently founded teams 

The historical data is found in the given CSV file with team name and year when it is founded. 

 
Final Exam
---
 
Basically the samething with Final_Project. You're importing a CSV into a database, but you're making a bootleg IMDB with live search using JS.

 
Final.py
---
 
Table inserting script. More details will follow on how to use.

**How To Use (GNU/Linux & MacOS Setup)**  

**Requirements**   

1. Before starting Make sure to have python3 installed
2. CSV is formatted correctly.(Commas should be aligned and there should be only data, so remove the column headers) If you don't remove column headers they will appear in the database.  

Installing the required mysql package, This will **NOT** work without it.
```console
$ pip install pymysql
```

On line 8. Enter the location of where your CSV file is located. 
```python
csvFile = 'pathToCSVFile/finalB.csv'
```
On line 15. Enter your database info. If your db has not pw left the quotes black i.e (password='')
```python
connection = pymysql.connect(host='localhost', user='root',password='',db='finalExam',charset='utf8mb4',cursorclass=pymysql.cursors.DictCursor)
```
On line 21. Add or subtracat the fields and values to fit your db. %s are for strings, though it hasn't affect it the SQL datatype is
an int().
```sql
sql = "INSERT INTO `movies` (`Title`,`Year`,`Director`) VALUES (%s,%s,%s)"
```
On line 24. add the number of fields in line 21 should match these, starting at 0.
```python
cursor.execute(sql,(line[0],line[1],line[2]))
```
With the CSV file and the **final.py** in the **SAME** directory run the following.

```console
$ python3 final.py

$ mysql -u root -p

$ USE db;

$ SELECT * from table;
```

your table should be populated.  
![Image of How db looks.](https://gitlab.com/Cruzny/cst/blob/7ac39e98eb1390aa0a588435c7821bb0b014ead2/Screen_Shot_2018-05-21_at_1.34.52_PM.png)  

Notes Regarding Databases
---
1. For obvious reasons, add your own database credentials.
2. Table Scheme may be different, you may be able to see how i set up the
tables by looking at the code.