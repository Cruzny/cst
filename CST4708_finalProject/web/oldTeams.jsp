<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
                <%
                    Class.forName("com.mysql.jdbc.Driver");
                    Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/final","root","pw");
                    Statement statement = connection.createStatement();
                    ResultSet rSet = statement.executeQuery("SELECT Team,Year FROM finalp ORDER BY year limit 5");
                    %>            
            <table border="2">
                <tr>
                    <td>Team Name</td>
                    <td>Year Founded</td>
                </tr>
                		    <%
                     while(rSet.next()){
                     %>
                     <tr><td><%= rSet.getString(1) %></td><td><%= rSet.getString(2) %></td></tr>
                     <%}
                    connection.close();
                    %>
    </body>
</html>
